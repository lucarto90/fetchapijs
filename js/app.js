// 
document.getElementById('txtBtn').addEventListener('click', cargarTXT);
document.getElementById('jsonBtn').addEventListener('click', cargarJSON);
document.getElementById('apiBtn').addEventListener('click', cargarREST);

function cargarTXT () {
    fetch('datos.txt')
        .then(res => res.text())
        .then(empleados => document.getElementById('resultado').innerHTML = empleados)
        .catch(error => console.log(error));
}

function cargarJSON () {
    fetch('empleados.json')
        .then(res => res.json())
        .then(data => {
            let html = '<ul>';

            data.forEach(element => {
                html += `
                    <li>${element.nombre}, ${element.puesto}</li>
                `;
            });

            html += '</ul>';

            document.getElementById('resultado').innerHTML = html;
        })
        .catch(error => console.log(error));
}

function cargarREST () {
    fetch('https://picsum.photos/list')
        .then(res => res.json())
        .then((imagenes) => {
            let html = '<ul>';

            imagenes.forEach((img) => {
                html += `
                    <li>
                        <a href="${img.post_url}" target="_blank">ver imagen</a>
                        ${img.author}
                    </li>
                `;
            })

            html += '</ul>';

            document.getElementById('resultado').innerHTML = html;
        })
        .catch(error => console.log(error));
}


/* ***********************************************************
 * ******************** Async Await **************************
 * ***********************************************************/

 async function leerTodos () {
     // esperar la respuesta
     const respuesta = await fetch('https://jsonplaceholder.typicode.com/todos');

     // Procede cuando la respuesta este hecha
     const datos = await respuesta.json();

     return datos;
 }

 leerTodos()
    .then(usuarios => console.log(usuarios));